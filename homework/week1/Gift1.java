import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.*;

/*

*BufferedReader

* |--LineNumberReader

* public int getLineNumber()获得当前行号。

* public voidsetLineNumber(int lineNumber)

*/

public class Gift1 {

    int statusIndex = 0;//当前动作状态的index
    int efcLineNum = 1;//当前动作 作用 到第几行结束（包含）

    public Gift1(int sc, int n){
        this.statusIndex = sc;
        this.efcLineNum = n;


    }

    public int get_statusCurrent(){
        return statusIndex;
    }

    public int get_efcLineNum(){
        return efcLineNum;
    }

    public void set_statusCurrent(int statusCurrent){
        this.statusIndex = statusCurrent;
    }

    public void  set_efcLineNum(int lineNum){
        this.efcLineNum = lineNum;
    }

    public static void main(String[] args) throws IOException {

        String[] arrStatus = {"numbersPeople","createPeople","setGiver","giveMount","setReceiver"}; //动作分类

        Gift1 gift1 = new Gift1(0,1);

        LineNumberReader lnr = new LineNumberReader(new FileReader("week1/gift1.in")); //输入文件

        //从0开始
        lnr.setLineNumber(0);
        String line = null;

        HashMap peopleBalance = new HashMap();
        while((line = lnr.readLine()) != null) {

            int statusCurrent = gift1.get_statusCurrent();
            int efcLineNum = gift1.get_efcLineNum();
            int currentLineNum = lnr.getLineNumber();
            int avrage = 0;
            int personNum = 0;
            switch (arrStatus[statusCurrent]){
                case "numbersPeople":
                    int lineInt = Integer.parseInt(line); //初始化 人名单 在txt文件中的 行数

                    if(efcLineNum == currentLineNum ){
                        gift1.set_statusCurrent(statusCurrent+1); //读完第一行 状态改变，期待输入 人名
                        gift1.set_efcLineNum(efcLineNum+lineInt);//输入人名 直到行数
                    }
                    break;
                case "createPeople":
                    peopleBalance.put(line, 0);
                    if(efcLineNum == currentLineNum ){
                        gift1.set_statusCurrent(statusCurrent+1);// txt 下一行 设置 给钱的人
                        gift1.set_efcLineNum(efcLineNum+1);// 期待行数 1
                    }
                    break;
                case "setGiver":
                    String giver = line;
                    if(efcLineNum == currentLineNum ){
                        gift1.set_statusCurrent(statusCurrent+1);//txt 下一行 设置给出去钱的 数额
                        gift1.set_efcLineNum(efcLineNum+1);// 期待行数 1
                    }
                    break;
                case "giveMount":
                    String[] temp = line.split(" ");
                    int total = Integer.parseInt(temp[0]);// 要分的总钱数
                    personNum = Integer.parseInt(temp[1]);// 得钱的人数
                    int selfLeft = total%personNum; //分钱后自己 给钱人自己剩余金额
                    avrage = (total-selfLeft)/personNum;// 每人得钱
                    peopleBalance.replace(line, selfLeft-total);// 给钱人 剩余的钱 放入自己账户
                    if(efcLineNum == currentLineNum ){
                        gift1.set_statusCurrent(statusCurrent+1);//txt 下一行 收钱人
                        gift1.set_efcLineNum(efcLineNum+personNum);// 期待行数 1
                    }
                    break;
                case "setReceiver":
                    int currentBlance = (int)peopleBalance.get("line");
                    peopleBalance.replace("line", currentBlance+avrage);
                    if(efcLineNum == currentLineNum ){
                        gift1.set_statusCurrent(statusCurrent-2);//txt 下一行 给钱人
                        gift1.set_efcLineNum(efcLineNum+personNum);// 期待行数 1
                    }
                    break;
            }
        }

        Iterator iterator = peopleBalance.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry entry = (Map.Entry) iterator.next();
            System.out.print(entry.getKey() + ": ");
            System.out.println(entry.getValue());
        }
        lnr.close();
    }
}