import java.util.*;


class ExtractCompressedString{
    public static void main(String[] args){
        /**
         * s = "3{a}2{bc}", return "aaabcbc".
         * s = "3{a2{c}}", return "accaccacc".
         * s = "2{abc}3{cd}ef", return "abcabccdcdcdef".
         *
         */
        String s = "10{a2{c}}ab";
        Stack<String> stcString = new Stack<String>();//‘语义断句’后的, 剔除数字后的，字符，如， {，a, {，c,},}...
        Stack<Integer> stcInt = new Stack<Integer>();//‘语义断句’后的, 数字，如上例中的 10,2,...
        Stack<String> stcLetter = new Stack<String>();//用于盛装 字母窜, 如上例中的 a, c, a, b
        Stack<String> stcOprt = new Stack<String>();//用于盛装 ”}”
        
        Character[] temp = new Character[s.length()];
        for(int i=0;i<s.length();i++){
            temp[i]=s.charAt(i);
        }

        String tempNum = "";
        String tempString = "";
        String tempPreType = ""; //为处理多位数字，记录当前读取char的type,
        String tempCurrType = ""; // 上一次读取char的type，便于比较，判断连位数字
        for (int i = 0; i < temp.length; i++) {
            if (Character.isDigit(temp[i])) {
                tempNum += temp[i]; //未压入栈,存储在临时变量
                tempCurrType = "digit";
            }else if (Character.isLetter(temp[i])){
                tempString += temp[i]; //未压入栈,存储在临时变量
                tempCurrType = "letter";

            }else if ('{' == temp[i]){
                tempCurrType = "";
                tempPreType = "";
                if(tempString != "") {
                    stcString.push(tempString);
                    tempString = "";
                }
                if(tempNum != "") {
                    stcInt.push(Integer.parseInt(tempNum));
                    tempNum = "";
                }
                stcString.push("{");
            }else if ('}' == temp[i]){
                tempCurrType = "";
                tempPreType = "";
                if(tempString != "") {
                    stcString.push(tempString);
                    tempString = "";
                }
                if(tempNum != "") {
                    stcInt.push(Integer.parseInt(tempNum));
                    tempNum = "";
                }
                stcString.push("}");
            }
            if(tempPreType=="") tempPreType = tempCurrType; //第一次读char[]
            if(tempCurrType!=tempPreType){
                switch (tempPreType){ //"{", "}" 如果相邻出现, 不会当做整体压入栈, 只处理数字和字母 相邻出现
                    case "digit":
                        stcInt.push(Integer.parseInt(tempNum));
                        tempNum = "";
                        break;
                    case "letter":
                        stcString.push(tempString);
                        tempString = "";
                        break;
                }
                tempPreType = tempCurrType;
            }
            if(i == temp.length-1){ //处理 以字母结尾的case, 如 2{a}eb
                if(tempString != "" ) stcString.push(tempString);
            }

        }

// 转存后stack形如: 10,{,a,2,{,c,},},ab
        String exString = ""; //解压后的字符串
        while(!stcString.empty()){
            String ele = stcString.pop();
            if ("{" != ele & "}" != ele){ // letter
                //stc_letter.push(ele);
                if (stcOprt.empty()){
                    exString = ele+exString;
                }else{
                    String tempStr = "";
                    if(!stcLetter.empty()) tempStr = stcLetter.pop();// 取当前缓存栈中的最上面一个字母窜.
                    tempStr = ele+tempStr;
                    stcLetter.push(tempStr);
                }
            }else if("}" == ele){
                stcOprt.push("}");

            }else if("{" == ele){
                stcOprt.pop(); //销毁一个'}'
                int times = stcInt.pop(); //取一个数值
                String tempExStr1 = stcLetter.pop();// 取当前缓存栈中的最上面一个字母窜.
                String tempExStr2 = "";

                for(int i=0; i<times; i++){
                    tempExStr2 += tempExStr1;
                }
                if(!stcOprt.empty()){
                    stcLetter.push(tempExStr2);
                }else{
                    exString = tempExStr2+exString;
                }
            }
        }

        System.out.println(exString);

    }
}
