import java.util.Arrays;
import java.util.Random;

public class Sort{

    public void main(String[] args){

        int[] arr = {0,1,3,2,4,7,6,5};

        bubbleSort(arr);
        System.out.println(Arrays.toString(arr));
        shuffle(arr);

        selctionSort(arr);
        System.out.println(Arrays.toString(arr));
        shuffle(arr);

        insertionSort(arr);
        System.out.println(Arrays.toString(arr));
        shuffle(arr);

        shellSort(arr);
        System.out.println(Arrays.toString(arr));
        shuffle(arr);

        int[] arr2 = mergeSort(arr);
        System.out.println(Arrays.toString(arr2));



    }


    void exChange(int[] arr, int i, int j){
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    void shuffle(int[] arr){
        Random rand = new Random();
        for(int i=0; i<arr.length-1; i++){
            int randInd = rand.nextInt(i);
            exChange(arr, i, randInd);
        }

    }

    void bubbleSort(int[] arr){
        boolean flag;//是否交换的标志
        for(int i=0; i<arr.length-1; i++){
            // 每次遍历标志位都要先置为false，才能判断后面的元素是否发生了交换
            flag = false;

            for(int j=arr.length-1; j>i; j--){ //选出该趟排序的最大值往后移动

                if(arr[j] < arr[j-1]){
                    exChange(arr, j, j-1);
                    flag = true;    //只要有发生了交换，flag就置为true
                }
            }
            // 判断标志位是否为false，如果为false，说明后面的元素已经有序，就直接return
            if(!flag) break;
        }
    }

    void selctionSort(int[] arr){

        for(int i=0; i<arr.length-1; i++){

            int minIndex = i; //当前最小元素索引
            for(int j=i+1;j<arr.length;j++){
                if(arr[j]<arr[minIndex]){
                    minIndex = j;
                }
            }
            if(minIndex != i){ //更换最小元素索引
                exChange(arr,i,minIndex);
            }
        }

    }

    void insertionSort(int[] arr){
        for(int i=0;i<arr.length-1;i++){
            for(int j=i+1;j>0;j--){
                if(arr[j] < arr[j-1]){
                    exChange(arr,j,j-1);//交换位置
                }else{         //不需要交换
                    break;
                }
            }
        }

    }

    void shellSort(int[] arr){
        int incre = arr.length;

        while(true){
            incre = incre/2; //incre 为增量

            for(int k = 0;k<incre;k++){    //根据增量分为若干子序列

                for(int i=k+incre;i<arr.length;i+=incre){

                    for(int j=i;j>k;j-=incre){
                        if(arr[j]<arr[j-incre]){
                            exChange(arr,j, j-incre);
                        }else{
                            break;
                        }
                    }
                }
            }

            if(incre == 1){
                break;
            }
        }

    }

    int[] mergeSort(int[] arr){
        if(arr.length <= 1) return arr;

        int num = arr.length >> 1;
        int[] leftArr = Arrays.copyOfRange(arr, 0, num);
        int[] rightArr = Arrays.copyOfRange(arr, num, arr.length);
        //System.out.println("split two array: " + Arrays.toString(leftArr) + " And " + Arrays.toString(rightArr));
        return mergeTwoArray(mergeSort(leftArr), mergeSort(rightArr));      //不断拆分为最小单元，再排序合并
    }


    private static int[] mergeTwoArray(int[] arr1, int[] arr2){
        int i = 0, j = 0, k = 0;
        int[] result = new int[arr1.length + arr2.length];  //申请额外的空间存储合并之后的数组
        while(i < arr1.length && j < arr2.length){      //选取两个序列中的较小值放入新数组
            if(arr1[i] <= arr2[j]){
                result[k++] = arr1[i++];
            }else{
                result[k++] = arr2[j++];
            }
        }
        while(i < arr1.length){     //序列1中多余的元素移入新数组
            result[k++] = arr1[i++];
        }
        while(j < arr2.length){     //序列2中多余的元素移入新数组
            result[k++] = arr2[j++];
        }
        //System.out.println("Merging: " + Arrays.toString(result));
        return result;
    }

}