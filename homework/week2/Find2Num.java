import java.util.*;

class Find2Num {
    public static void main(String[] args){
        int[] arr = {10, 2, 3, 1, 2, 4, -1, 7, 5, 7};
        int sum = 9;
        /*
        * 把给定数组转储为 hashtable，键为 数组元素， 值为 出现次数
        *
        * */
        Hashtable<Integer,Integer> eles_hash = new Hashtable<>();
        for(int arr_ele: arr){
            if(eles_hash.containsKey(arr_ele)) {
                int ele_times = eles_hash.get(arr_ele)+1;
                eles_hash.replace(arr_ele,ele_times);
            }else {
                eles_hash.put(arr_ele, 1);
            }
        }

        //遍历hashtable 找出 两元素 之和 =9 的组合
        Enumeration eles_hash_keys = eles_hash.keys();

        while( eles_hash_keys.hasMoreElements() ){
            int current_ele_key = (int) eles_hash_keys.nextElement();
            int expected_ele_key = sum-current_ele_key; // 计算得出 期待 的key值

            //开始组合：
            //先挑选一个 元素
            int current_ele_key_value = eles_hash.get(current_ele_key)-1; // 用过的 key， 对应值（元素个数） -1
            if(current_ele_key_value == 0 ){
                eles_hash.remove(current_ele_key); // 若值为0， 则说明 用玩了， remove掉此元素
            }else {
                eles_hash.replace(current_ele_key,current_ele_key_value);// 若没用完， 则更新 元素个数
            }
            //如果有对应的 元素
            if(eles_hash.containsKey(expected_ele_key)){ // 若有 期待的 key

                int expected_ele_key_value = eles_hash.get(expected_ele_key)-1;// 用过的 key， 对应值（元素个数） -1
                if(expected_ele_key_value == 0){
                    eles_hash.remove(expected_ele_key);// 若值为0， 则说明 用玩了， remove掉此元素
                }else{
                    eles_hash.replace(expected_ele_key,expected_ele_key_value);// 若没用完， 则更新 元素个数
                }
                System.out.printf("[%s,%s]",current_ele_key,expected_ele_key); // 输出组合
            }

        }
    }
}